import Vue from 'vue';
import Vuex from 'vuex';

import _mutations from './../mutations.js';
import _actions from './../actions.js';
import _getters from './../getters.js';

Vue.use(Vuex);

export const getters = _getters;
export const mutations = _mutations;
export const actions = _actions;

export const state = {
    authError: false,
    authSuccess: false,
    authErrorMessages: [],
    authSuccessMessages: [],
    conversations: [
      { "title": "Jeahh its cool- no graphcool",
        "id": "cjhrrzpsfyjlg01046bk55neo",
        "messages": [
          { 
            "id": 1,
            "content": 'Test Message',
            "createdBy": {
              "username": 'test'
            },
            "created_at": '2018-05-16 23:25:29',
            "updated_at": '2018-05-16 23:25:29'
          }
        ],
        "members": [
            {
              "id": "cjhoy2fi7538a0186ro87pn08",
              "username": "christa"
            },
            {
              "id": "cjhp37d4m6d3w01012gbc702q",
              "username": "test"
            }
          ]
        ,
        "createdBy": 
            {
              "id": "cjhoy2fi7538a0186ro87pn08",
              "username": "christa"
            }
        }
      ],
    currentConversation: { "title": "Jeahh its cool- no graphcool",
        "id": "cjhrrzpsfyjlg01046bk55neo",
        "messages": [
          { 
            "id": 1,
            "content": 'Test Message',
            "createdBy": {
              "username": 'test'
            },
            "created_at": '2018-05-16 23:25:29',
            "updated_at": '2018-05-16 23:25:29'
          }
        ],
        "members": [
            {
              "id": "cjhoy2fi7538a0186ro87pn08",
              "username": "christa"
            },
            {
              "id": "cjhp37d4m6d3w01012gbc702q",
              "username": "test"
            }
          ]
        ,
        "createdBy": 
            {
              "id": "cjhoy2fi7538a0186ro87pn08",
              "username": "christa"
            }
        },
    currentUser: 'test',
    users: [
      {
        "id": "cjhoy2fi7538a0186ro87pn08",
        "username": "christa",
        "password": "chrisi"
      },
      {
        "id": "cjhp37d4m6d3w01012gbc702q",
        "username": "test",
        "password": "chrisi"
      },
      {
        "id": "cjhp38gsz75vh01180mmbdtj8",
        "username": "chrisi",
        "password": "chrisi"
      },
      {
        "id": "cjhyolw2fd97v0169to8g7xqu",
        "username": "Christina",
        "password": "chrisi"
      }
    ]
  
};

// eslint-disable-next-line no-underscore-dangle
export function __createMocks(custom = { getters: {}, mutations: {}, actions: {}, state: {} }) {
  const mockGetters = Object.assign({}, getters, custom.getters);
  const mockMutations = Object.assign({}, mutations, custom.mutations);
  const mockActions = Object.assign({}, actions, custom.actions);
  const mockState = Object.assign({}, state, custom.state);

  return {
    getters: mockGetters,
    mutations: mockMutations,
    actions: mockActions,
    state: mockState,
    store: new Vuex.Store({
      getters: mockGetters,
      mutations: mockMutations,
      actions: mockActions,
      state: mockState,
    }),
  };
}

export const store = __createMocks().store;