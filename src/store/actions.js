import gql from 'graphql-tag'
import apollo from '../apollo.js'

import actionTypes from './action-types.js'
import mutationTypes from './mutation-types.js'

export default {
  async [actionTypes.GET_CONVERSATION_BY_ID] ({ commit }, id) {
    const { data: { Conversation } } = await apollo.query({
      query: gql`
      query Conversation($id: ID!) {
        Conversation(id: $id) {
          id,
          title,
          members { username },
          messages {
            content,
            createdBy { username },
            createdAt
          },
          createdBy { username }
        }
      }
      `,
      variables: { id },
      fetchPolicy: 'no-cache'
    })
    commit(mutationTypes.SET_CONVERSATION, Conversation)
    commit(mutationTypes.SET_CURRENT_CONVERSATION, Conversation)
  },
  async [actionTypes.GET_CONVERSATIONS] ({ commit, state }) {
    let username = state.currentUser

    if (username) {
      const { data: { allConversations } } = await apollo.query({
        query: gql`
        query allConversations($username: String!) {
          allConversations(filter: { members_some: { username: $username }}) {
            id,
            title,
            members { username },
            messages {
              content,
              createdBy { username },
              createdAt
            },
            createdBy { username }
          }
        }
        `,
        variables: { username },
        fetchPolicy: 'no-cache'
      })
      commit(mutationTypes.SET_CONVERSATIONS, allConversations)
      commit(mutationTypes.SET_CURRENT_CONVERSATION, allConversations[0])
    }
  },
  async [actionTypes.CREATE_CONVERSATION] ({ dispatch }, credentials) {
    const { data: { newConversation } } = await apollo.mutate({
      mutation: gql`
        mutation newConversation($title: String!, $membersUsernames: [String!]) {
          newConversation(title: $title, membersUsernames: $membersUsernames) {
            id
          }
        }
      `,
      variables: credentials
    })

    dispatch('GET_CONVERSATION_BY_ID', newConversation.id)
  },
  async [actionTypes.DELETE_CONVERSATION] ({ commit, dispatch }, id) {
    await apollo.mutate({
      mutation: gql`
        mutation deleteConversation($id: ID!) {
          deleteConversation(id: $id) {
            id
          }
        }
      `,
      variables: { id }
    })

    commit(mutationTypes.DELETE_CONVERSATION, id)
    dispatch(actionTypes.GET_CONVERSATIONS)
  },
  async [actionTypes.SEND_MESSAGE] ({ dispatch }, credentials) {
    await apollo.mutate({
      mutation: gql`
        mutation newMessage($content: String!, $conversationId: String!) {
          newMessage(content: $content, conversationId: $conversationId) {
            id
          }
        }
      `,
      variables: credentials
    })
    dispatch('GET_CONVERSATION_BY_ID', credentials.conversationId)
  },
  async [actionTypes.GET_USERS] ({ commit }) {
    const { data: { allUsers } } = await apollo.query({
      query: gql`
        query {
          allUsers {
            id,
            username
          }
        }
      `,
      fetchPolicy: 'no-cache'
    })
    commit(mutationTypes.SET_USERS, allUsers)
  },
  async [actionTypes.LOGIN] ({ commit }, credentials) {
    const { data: { authenticateUser } } = await apollo.mutate({
      mutation: gql`
        mutation authenticateUser($username: String!, $password: String!) {
          authenticateUser(username: $username, password: $password) {
            token
          }
        }
      `,
      variables: credentials
    })
    const { token } = authenticateUser
    localStorage.setItem('polliChat_username', credentials.username)
    localStorage.setItem('polliChat_userToken', token)
    commit(mutationTypes.LOGIN, credentials.username)
  },
  [actionTypes.LOGOUT] ({ commit }) {
    localStorage.removeItem('polliChat_username')
    localStorage.removeItem('polliChat_userToken')
    commit(mutationTypes.LOGOUT)
    commit(mutationTypes.CLEAR_CONVERSATIONS)
  },
  async [actionTypes.REGISTER] ({ commit }, credentials) {
    await apollo.mutate({
      mutation: gql`
        mutation signupUser($username: String!, $password: String!) {
          signupUser(username: $username, password: $password) {
            id,
            token
          }
        }
      `,
      variables: credentials
    })
  }
}
