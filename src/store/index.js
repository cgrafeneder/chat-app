import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions.js'
import mutations from './mutations.js'
import getters from './getters.js'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    authError: false,
    authSuccess: false,
    authErrorMessages: [],
    authSuccessMessages: [],
    conversations: [],
    currentConversation: [],
    currentUser: localStorage.getItem('polliChat_username'),
    users: []
  },
  actions,
  mutations,
  getters
})

export default store
