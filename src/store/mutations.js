import Vue from 'Vue'

import mutationTypes from './mutation-types.js'

export default {
  [mutationTypes.SET_CONVERSATION]: (state, conversation) => {
    let included = false
    for (let conversationKey in state.conversations) {
      if (state.conversations[conversationKey].id === conversation.id) {
        Vue.set(state.conversations, conversationKey, conversation)
        included = true
      }
    }
    if (!included) state.conversations.push(conversation)
  },
  [mutationTypes.SET_CONVERSATIONS]: (state, conversations) => {
    state.conversations = [...conversations]
  },
  [mutationTypes.SET_CURRENT_CONVERSATION]: (state, conversation) => {
    state.currentConversation = conversation
  },
  [mutationTypes.DELETE_CONVERSATION]: (state, id) => {
    let index = state.conversations.map(c => c.id).indexOf(id)
    state.conversations.splice(index, 1)
  },
  [mutationTypes.CLEAR_CONVERSATIONS]: (state) => {
    state.conversations = []
  },
  [mutationTypes.LOGIN]: (state, username) => {
    state.currentUser = username
  },
  [mutationTypes.LOGOUT]: (state) => {
    state.currentUser = null
  },
  [mutationTypes.SET_USERS]: (state, users) => {
    state.users = users
  },
  [mutationTypes.SET_AUTH_ERROR]: (state, message) => {
    state.authErrorMessages.push(message)
  },
  [mutationTypes.SET_AUTH_SUCCESS]: (state, message) => {
    state.authSuccessMessages.push(message)
  },
  [mutationTypes.RESET_AUTH_MESSAGES]: (state) => {
    state.authError = false
    state.authSuccess = false
    state.authErrorMessages = []
    state.authSuccessMessages = []
  }
}
