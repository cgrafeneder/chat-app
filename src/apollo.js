import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { createHttpLink } from 'apollo-link-http'
import { onError } from 'apollo-link-error'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'
import fetch from 'unfetch'

const httpLink = createHttpLink({
  uri: 'https://api.graph.cool/simple/v1/cjhj1cvls8kfq0191sbukftwg',
  fetch: fetch
})

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('polliChat_userToken')
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
})

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) =>
      // eslint-disable-next-line no-console
      console.error(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
    )
  }
  if (networkError) {
    // eslint-disable-next-line no-console
    console.error(`[Network error]: ${networkError}`)
  }
})

const link = ApolloLink.from([
  authLink,
  errorLink,
  httpLink
])

export default new ApolloClient({
  link,
  cache: new InMemoryCache(),
  connectToDevTools: true
})
