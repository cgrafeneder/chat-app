import { fromEvent, FunctionEvent } from 'graphcool-lib'
import { GraphQLClient } from 'graphql-request'

interface Message {
  id: string
}

interface User {
  id: string
}

interface EventData {
  content: string
  conversationId: string
}

const SALT_ROUNDS = 10

export default async (event: FunctionEvent<EventData>) => {
  console.log(event)

  try {
    const graphcool = fromEvent(event)
    const api = graphcool.api('simple/v1')

    // -------------------------------
    // get current user (for createdBy)
    // no logged in user
    if (!event.context.auth || !event.context.auth.nodeId) {
      return { error: "User is not logged" }
    }
    const userId = event.context.auth.nodeId
    const user: User = await getUserById(api, userId).then(r => r.User)
    if (!user || !user.id) {
      return { error: "User is not logged in or does not exist!" }
    }
    // -------------------------------

    const { content, conversationId } = event.data
    
    // create new message
    const messageId = await createGraphcoolMessage(api, content, user.id, conversationId)
    
    return { data: { id: messageId } }
  } catch (e) {
    console.log(e)
    return { error: 'An unexpected error occured during creating a message.' }
  }
}

// -----------------------------
// USER by id 

async function getUserById(api: GraphQLClient, id: string): Promise<{ User }> {
  const query = `
    query getUser($id: ID!) {
      User(id: $id) {
        id
      }
    }
  `

  const variables = {
    id
  }

  return api.request<{ User }>(query, variables)
}

// -----------------------------
// MESSAGE 
async function createGraphcoolMessage(api: GraphQLClient, content: string, createdById: string, conversationId: string): Promise<string> {
  const mutation = `
    mutation createGraphcoolMessage($content: String!, $createdById: ID!, $conversationId: ID!) {
      createMessage(
        content: $content,
        createdById: $createdById,
        conversationId: $conversationId
      ) {
        id
      }
    }
  `
  const variables = {
    content,
    createdById: createdById,
    conversationId: conversationId,
  }

  return api.request<{ createMessage: Message }>(mutation, variables)
    .then(r => r.createMessage.id)
}

