import { fromEvent, FunctionEvent } from 'graphcool-lib'
import { GraphQLClient } from 'graphql-request'

interface Conversation {
  id: string
  title: string
  createdById: string
}

interface User {
  id: string
  createdConversationsConversation: string
}

interface EventData {
  title: string
  membersUsernames: [string]
}

interface CreatorOnConversation {
  createdByUser: User
  createdConversationsConversation: Conversation
}

interface UserOnConversation {
  membersUser: User
  conversationsConversation: Conversation
}


const SALT_ROUNDS = 10

export default async (event: FunctionEvent<EventData>) => {
  console.log(event)

  try {
    const graphcool = fromEvent(event)
    const api = graphcool.api('simple/v1')

    // -------------------------------
    // get current user (for createdBy)
    // no logged in user
    if (!event.context.auth || !event.context.auth.nodeId) {
      return { error: "User is not logged" }
    }
    const userId = event.context.auth.nodeId
    const user: User = await getUserById(api, userId).then(r => r.User)
    if (!user || !user.id) {
      return { error: "User is not logged in or does not exist!" }
    }
    // -------------------------------

    const { title, membersUsernames } = event.data

    // get Userids instead of usernames
    for (var i = membersUsernames.length - 1; i >= 0; i--) {
      const id: string = await getUser(api, membersUsernames[i]).then(r => r.User.id)
      membersUsernames[i] = id
    }

    // create new conversation
    const conversationId = await createGraphcoolConversation(api, title, user.id, membersUsernames)

    return { data: { id: conversationId } }
  } catch (e) {
    console.log(e)
    return { error: 'An unexpected error occured during creating a conversation.' }
  }
}

// -----------------------------
// USER by id
async function getUser(api: GraphQLClient, username: string): Promise<{ User }> {
  const query = `
    query getUser($username: String!) {
      User(username: $username) {
        id
      }
    }
  `

  const variables = {
    username,
  }

  return api.request<{ User }>(query, variables)
}

async function getUserById(api: GraphQLClient, id: string): Promise<{ User }> {
  const query = `
    query getUser($id: ID!) {
      User(id: $id) {
        id
      }
    }
  `

  const variables = {
    id
  }

  return api.request<{ User }>(query, variables)
}

// -----------------------------
// CONVERSATION
async function createGraphcoolConversation(api: GraphQLClient, title: string, createdById: string, membersIds: [string]): Promise<string> {
  const mutation = `
    mutation createGraphcoolConversation($title: String!, $createdById: ID!, $membersIds: [ID!]) {
      createConversation(
        title: $title,
        createdById: $createdById,
        membersIds: $membersIds
      ) {
        id
      }
    }
  `
  const variables = {
    title,
    createdById: createdById,
    membersIds: membersIds,
  }

  return api.request<{ createConversation: Conversation }>(mutation, variables)
    .then(r => r.createConversation.id)
}
