# Polli Chat Application
> A chat application with Vue.js and GraphQL

Our project is about a chat app with single and group conversations. When you go the page you will have to enter a username and then you are logged in. So you are visible for the other users in the application.

## Application Parts
Our chat app consists of three areas:
1. on the left side are shown all existing chat conversations  
2. in the middle is the chat area where all messages are shown and you can type in your new message  
3. Button/Area to add conversations (either single or group) by adding user(names)  


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests (only nightwatch)
npm run e2e

# run cucumber/nightwatch tests
npm run nightwatch

# run all tests (unit, nightwatch, cucumber)
npm test
```

## Test Users in Application
password always ```asdf```  
* christa  
* anna  
* jonas  
* chrisi  
* test  

* cucumber (password: cucumber) -> for testing

## Project Members
Christina Grafeneder (S1710629009)  
Christa Höglinger (S1710629010)
