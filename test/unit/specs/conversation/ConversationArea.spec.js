import Vue from 'vue'
import Vuex from 'vuex'
import { __createMocks as createStoreMocks, state } from '@/store/_mocks_'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils'

import ConversationArea from '@/components/conversation/ConversationArea'

jest.mock('@/store/_mocks_')
const localVue = createLocalVue()
localVue.use(Vuex)

describe('ConversationArea', () => {
	let storeMocks
	let wrapper

	beforeEach(() => {
	  storeMocks = createStoreMocks()

	  wrapper = mount(ConversationArea, {
	    store: storeMocks.store, 
      localVue
	  })
	})

  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('shows no conversation area when no conversation exists', () => {
    // mount with no conversations
    const _state =  Object.assign({}, state);
    _state.conversations = []
    const storeMocksEmpty = createStoreMocks({state: _state})
    const wrapperEmpty = mount(ConversationArea, {
      store: storeMocksEmpty.store, 
      localVue
    })

    expect(wrapperEmpty.find('.conversation-area').exists()).toBe(false)
  })

  it('shows all conversations', () => {
    const list = wrapper.find('.conversation-list').find("ul")

    expect(list.exists()).toBe(true)
    expect(list.findAll('span.conversation-title').at(0).text()).toBe(
      "Jeahh its cool- no graphcool")
    expect(list.findAll('span.conversation-title').length).toBe(1)
  })


  

})