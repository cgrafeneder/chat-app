import Vue from 'vue'
import Vuex from 'vuex'
import { __createMocks as createStoreMocks } from '@/store/_mocks_'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils'

import ConversationNewModal from '@/components/conversation/ConversationNewModal'

jest.mock('@/store/_mocks_')
const localVue = createLocalVue()
localVue.use(Vuex)

describe('ConversationNewModal', () => {
  let storeMocks
  let wrapper

  beforeEach(() => {
    storeMocks = createStoreMocks()

    wrapper = shallowMount(ConversationNewModal, {
      store: storeMocks.store, localVue,attachToDocument: true
    })
  })

  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('show represents main layout of a modal', () => {
    expect(wrapper.find('div.modal').exists()).toBe(true)
    expect(wrapper.find('div.modal-background').exists()).toEqual(true)
    expect(wrapper.find('div.modal-content').exists()).toEqual(true)
    expect(wrapper.find('button.modal-close.is-large').exists()).toEqual(true)

    expect(wrapper.find('h1.modal-card-title').exists()).toEqual(true)
    expect(wrapper.find('section.modal-card-body').exists()).toEqual(true)
  })

  it('show new conversation form', () => {
    const body = wrapper.find('.modal-card-body')
    expect(body.find('input#users-autocomplete').exists()).toBe(true)

    expect(body.find('.users').exists()).toBe(true)
    const users_control = body.find('.users').findAll(".control.checkradio-area")
    expect(users_control.length).toBe(4)
    expect(users_control.at(0).find('.add-participant').exists()).toBe(true)

    expect(wrapper.find('.button.is-primary').text()).toBe("Create Conversation")
    expect(wrapper.find('.button.cancel').text()).toBe("Cancel")
  })

  it('return error when no input', () => {
    const button = wrapper.find('.button.is-primary')
    button.trigger('click')

    expect(wrapper.find('.help.is-danger').exists()).toEqual(true)
    const error = wrapper.findAll('.help.is-danger')
    expect(error.length).toEqual(1)
    expect(error.at(0).text()).toEqual("Please select participant(s) for this conversation!")
  })

})