import Vue from 'vue'
import Vuex from 'vuex'
import { __createMocks as createStoreMocks } from '@/store/_mocks_'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils'

import AuthModal from '@/components/auth/AuthModal'

jest.mock('@/store/_mocks_')
const localVue = createLocalVue()
localVue.use(Vuex)

describe('AuthModal', () => {
	let storeMocks
	let wrapper

	beforeEach(() => {
	  storeMocks = createStoreMocks()

	  wrapper = shallowMount(AuthModal, {
	    store: storeMocks.store, localVue,
	  })
	})

  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('show represents main layout of a modal', () => {
    expect(wrapper.find('div.modal').exists()).toBe(true)
    expect(wrapper.find('div.modal-background').exists()).toEqual(true)
    expect(wrapper.find('div.modal-content').exists()).toEqual(true)
    expect(wrapper.find('button.modal-close.is-large').exists()).toEqual(true)

    expect(wrapper.find('h1.modal-card-title').exists()).toEqual(true)
    expect(wrapper.find('section.modal-card-body').exists()).toEqual(true)
  })

	it('show login view with a toggle button to register view', () => {
    expect(wrapper.find('h1').text()).toEqual("Who's there?")

    const usernameWrapper = wrapper.find('.field.auth-username')
    expect(usernameWrapper.exists()).toBe(true)
    expect(usernameWrapper.find('input#auth-username').exists()).toBe(true)
    expect(usernameWrapper.find('i.fas.fa-user').exists()).toBe(true)

    const passwordWrapper = wrapper.find('.field.auth-password')
    expect(passwordWrapper.exists()).toBe(true)
    expect(passwordWrapper.find('input#auth-password').exists()).toBe(true)
    expect(passwordWrapper.find('i.fas.fa-lock').exists()).toBe(true)

    const passwordConfirmWrapper = wrapper.find('.field.auth-password-confirm')
    expect(passwordConfirmWrapper.exists()).toBe(false)

    expect(wrapper.find('button.is-primary').text()).toBe("Login")
    expect(wrapper.find('button.is-text').text()).toBe("Register")
  })

  it('switch to register view when click on "register" button and back', () => {
  	const register_button = wrapper.find('button.is-text')
    register_button.trigger('click')

    expect(wrapper.find('h1').text()).toEqual("Please input your data to register")
    expect(wrapper.find('input#auth-username').exists()).toBe(true)
		expect(wrapper.find('input#auth-password').exists()).toBe(true)
		expect(wrapper.find('input#auth-password-confirm').exists()).toBe(true)

		expect(wrapper.find('button.is-text').text()).toEqual("Login")
		expect(wrapper.find('button.is-primary').text()).toEqual("Register")

		const login_button = wrapper.find('button.is-text')
    login_button.trigger('click')
    expect(wrapper.find('button.is-primary').text()).toEqual("Login")
  	expect(wrapper.find('button.is-text').text()).toEqual("Register")
  })

  it('return error when no credentials in login form', () => {
  	const button = wrapper.find('button.is-primary')
    button.trigger('click')

    expect(wrapper.find('.help.is-danger').exists()).toEqual(true)
    const error = wrapper.findAll('.help.is-danger')
    expect(error.length).toEqual(2)
    expect(error.at(0).text()).toEqual("Please input your username!")
    expect(error.at(1).text()).toEqual("Please input your password!")
  })

  it('return error when no input in register form', () => {
    const register_button = wrapper.find('button.is-text')
    register_button.trigger('click')
    const button = wrapper.find('button.is-primary')
    button.trigger('click')

    expect(wrapper.find('.help.is-danger').exists()).toEqual(true)
    const error = wrapper.findAll('.help.is-danger')
    expect(error.length).toEqual(2)
    expect(error.at(0).text()).toEqual("Please input a username!")
    expect(error.at(1).text()).toEqual("Please input a password!")
  })

})