import Vue from 'vue'
import Vuex from 'vuex'
import { __createMocks as createStoreMocks } from '@/store/_mocks_'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils'

import AuthInfo from '@/components/auth/AuthInfo'

// Tell Jest to use the mock
// implementation of the store.
jest.mock('@/store/_mocks_')
const localVue = createLocalVue()
localVue.use(Vuex)

describe('AuthInfo', () => {
	let storeMocks
	let wrapper

	beforeEach(() => {
	  // Create a fresh store and wrapper
	  // instance for every test case.
	  storeMocks = createStoreMocks()

	  wrapper = mount(AuthInfo, {
	    store: storeMocks.store, localVue,
	  })
	})
	
	it('only shows information for the user', () => {
		expect(wrapper.find('.navbar-user-button').exists()).toBe(false)
		expect(wrapper.find('.modal').exists()).toBe(false)

	  expect(wrapper.find('.container').exists()).toBe(true)
	  expect(wrapper.find('.title').text()).toEqual('Please log in to start to plan your adventure!')
	})

 	it('renders button for login', () => {
 		const button = wrapper.find('.button.is-primary')
		expect(button.isVisible()).toBe(true)
	  expect(button.text()).toEqual('Login')
	  expect(wrapper.find('.modal').exists()).toBe(false)
	  
	})

	it('show auth modal when button "login" is clicked', () => {
    const button = wrapper.find('.button.is-primary')
    button.trigger('click')
    expect(wrapper.find('.modal').exists()).toBe(true)
  })

})