import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import router from '@/router'

import { __createMocks as createStoreMocks, state } from '@/store/_mocks_'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils'

import LayoutDefault from '@/components/layouts/LayoutDefault'

jest.mock('@/store/_mocks_')
const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(VueRouter)

describe('LayoutDefault', () => {
	let storeMocks
	let wrapper

	beforeEach(() => {
	  storeMocks = createStoreMocks()

	  wrapper = mount(LayoutDefault, {
      router: router,
	    store: storeMocks.store, 
      localVue
	  })
	})

  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('shows main layout', () => {
  	expect(wrapper.find(".layout-default").exists()).toBe(true)
  	expect(wrapper.find("header").exists()).toBe(true)
    expect(wrapper.find("main").exists()).toBe(true)
    expect(wrapper.find("footer").exists()).toBe(true)
  })

})