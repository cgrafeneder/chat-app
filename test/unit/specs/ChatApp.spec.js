 import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import router from '@/router'

import { __createMocks as createStoreMocks, state } from '@/store/_mocks_'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils'

import ChatApp from '@/components/ChatApp'

jest.mock('@/store/_mocks_')
const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(VueRouter)

describe('ChatApp', () => {
	let storeMocks
	let wrapper

	beforeEach(() => {
	  storeMocks = createStoreMocks()

	  wrapper = mount(ChatApp, {
	  	router: router,
	    store: storeMocks.store, 
      localVue
	  })
	})

  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('shows main layout', () => {
  	expect(wrapper.find(".main-area").exists()).toBe(true)
  	expect(wrapper.find(".new-conversation-area").exists()).toBe(true)

  	const user_button = wrapper.find(".navbar-user-button .button.is-primary")
  	expect(user_button.exists()).toBe(true)
  	expect(user_button.text()).toBe(storeMocks.store.state.currentUser)
  	
  	const new_conversation_area = wrapper.find(".new-conversation-area .button.is-primary")
  	expect(new_conversation_area.exists()).toBe(true)
  	expect(new_conversation_area.text()).toBe("Start new Conversation")

  	expect(wrapper.find('.chat-area').exists()).toBe(true)
  })

  it('shows no conversation area when no conversation exists', () => {
    // mount with no conversations
    const _state =  Object.assign({}, state);
    _state.conversations = []
    const storeMocksEmpty = createStoreMocks({state: _state})
    const wrapperEmpty = mount(ChatApp, {
    	router: router,
      store: storeMocksEmpty.store, 
      localVue
    })

    expect(wrapperEmpty.find('.conversation--wrapper').exists()).toBe(false)
    expect(wrapperEmpty.find('.chat-area h1.info-no-conversations').exists()).toBe(true)
    expect(wrapperEmpty.find('.chat-area h1.info-no-conversations').text()).toBe("So far no conversations available - please create one!")
  })

  it('shows conversations area', () => {
		expect(wrapper.find('.conversation-wrapper').exists()).toBe(true)
  	expect(wrapper.find('.conversation-area').exists()).toBe(true)
  	expect(wrapper.find('.chat-area h1.info-no-conversations').exists()).toBe(false)
  })

})