import Vue from 'vue'
import Vuex from 'vuex'
import { __createMocks as createStoreMocks, state } from '@/store/_mocks_'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils'

import MessageArea from '@/components/message/MessageArea'

jest.mock('@/store/_mocks_')
const localVue = createLocalVue()
localVue.use(Vuex)

describe('MessageArea', () => {
	let storeMocks
	let wrapper

	beforeEach(() => {
	  storeMocks = createStoreMocks()

	  wrapper = mount(MessageArea, {
	    store: storeMocks.store, 
      localVue
	  })
	})

  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

    it('shows all messages', () => {
    const list = wrapper.find('.chat-wrapper .chat-conversation').find("ul")

    expect(list.exists()).toBe(true)
    expect(list.findAll('div.message-text a').at(0).text()).toBe("Test Message")
    expect(list.findAll('div.message-text a').length).toBe(1)
  })

  it('shows empty message when no messages exists', () => {
    // mount with no conversations
    const _state =  Object.assign({}, state);
    _state.currentConversation.messages = []
    const storeMocksEmpty = createStoreMocks({state: _state})
    const wrapperEmpty = mount(MessageArea, {
      store: storeMocksEmpty.store, 
      localVue
    })

    expect(wrapperEmpty.find('.info-no-messages').text()).toBe("So far no messages have been posted - be the first!")
  })

})