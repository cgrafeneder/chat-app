import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import router from '@/router'

import { __createMocks as createStoreMocks, state } from '@/store/_mocks_'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils'

import AppHeader from '@/components/app/AppHeader'

jest.mock('@/store/_mocks_')
const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(VueRouter)

describe('AppHeader', () => {
	let storeMocks
	let wrapper

	beforeEach(() => {
	  storeMocks = createStoreMocks()

	  wrapper = mount(AppHeader, {
      router: router,
	    store: storeMocks.store, 
      localVue
	  })
	})

  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('shows main layout', () => {
    expect(wrapper.find("header").exists()).toBe(true)
    expect(wrapper.find(".hero").exists()).toBe(true)
    expect(wrapper.find(".container.navbar-polli").exists()).toBe(true)
    expect(wrapper.find("h1.title").text()).toBe("Polli Chat")
  
    expect(wrapper.find(".navbar-user-button").exists()).toBe(true)
  })

  it('shows user button when user is logged in', () => {
    const navbar_user_wrapper = wrapper.find(".navbar-user-button #logged-in-user")
    expect(navbar_user_wrapper.exists()).toBe(true)

    const navbar_user_button = navbar_user_wrapper.find("button")
    expect(navbar_user_button.exists()).toBe(true)
    expect(navbar_user_button.find("i.fas.fa-user-circle").exists()).toBe(true)
    expect(navbar_user_wrapper.find("#logged-in-user button").text()).toBe(storeMocks.store.state.currentUser)

    expect(navbar_user_wrapper.find(".dropdown-trigger").exists()).toBe(true)
    expect(navbar_user_wrapper.find(".dropdown-menu").exists()).toBe(true)   
    expect(navbar_user_wrapper.find(".dropdown-menu a.router-link-active").text()).toBe("Logout")
  })

  it('shows logout button when click on username', () => {
    expect(wrapper.find("#logged-in-user.is-active").exists()).toBe(false)
    const navbar_user_button = wrapper.find("#logged-in-user button")
    navbar_user_button.trigger("click")
    expect(wrapper.find("#logged-in-user.is-active").exists()).toBe(true)
  })

  it('shows no user button when user is not logged in', () => {
    // mount with no current user
    const _state =  Object.assign({}, state);
    _state.currentUser = undefined
    const storeMocksEmpty = createStoreMocks({state: _state})
    const wrapperEmpty = mount(AppHeader, {
      router: router,
      store: storeMocksEmpty.store, 
      localVue
    })
    expect(wrapperEmpty.find(".navbar-user-button").exists()).toBe(true)
    expect(wrapperEmpty.find("#logged-in-user").exists()).toBe(false)

  })

})