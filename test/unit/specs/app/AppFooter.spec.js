import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import router from '@/router'

import { __createMocks as createStoreMocks, state } from '@/store/_mocks_'
import { mount, shallowMount, createLocalVue } from '@vue/test-utils'

import AppFooter from '@/components/app/AppFooter'

jest.mock('@/store/_mocks_')
const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(VueRouter)

describe('AppFooter', () => {
	let storeMocks
	let wrapper

	beforeEach(() => {
	  storeMocks = createStoreMocks()

	  wrapper = mount(AppFooter, {
      router: router,
	    store: storeMocks.store, 
      localVue
	  })
	})

  it('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('shows main layout', () => {
    expect(wrapper.find("footer").exists()).toBe(true)
    expect(wrapper.find(".container").exists()).toBe(true)

    expect(wrapper.find(".content").exists()).toBe(true)
     expect(wrapper.find(".content p").text()).toBe("Polli Chat Application by Christina Grafeneder and Christa Höglinger.")
  })

})