require('nightwatch-cucumber')({
  cucumberArgs: [
    '--require', 'test/cucumber/step_definitions',
    '--format', 'node_modules/cucumber-pretty',
    'test/cucumber/features',
  ],
});

const args = ['no-sandbox'];
if (process.argv.includes('--headless')) {
  args.push('headless');
}

module.exports = {
  globals_path: 'test/cucumber/conf/globals.js',
  selenium: {
    start_process: false,
  },

  test_settings: {
    default: {
      launch_url: 'http://localhost:8080',
      selenium_port: 9515,
      selenium_host: '127.0.0.1',
      default_path_prefix: '',

      desiredCapabilities: {
        browserName: 'chrome',
        chromeOptions: {
          args,
        },
        acceptSslCerts: true,
      },
    },
  },
};