Feature: Conversation
# Test Components Conversation

  Scenario: User sees the conversation area
    Given I open the `start page`
    And   I click the `login button`
    And   I enter a valid username "cucumber" into `username input`
    And   I enter a valid password "cucumber" into `password input`
    And   I click the `submit button` and wait some time
    And   I see the conversation area

  Scenario: User see a new conversation modal
    When  I click the `new conversation button`
    And   I see the `modal title` placeholder is "What's the name of the conversation?"