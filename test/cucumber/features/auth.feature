Feature: Authenticate
# Test Components Authentication Info and Modal

  Scenario: User visits login page
    Given I open the `start page`
    Then  I see the `nav-bar title` is "Polli Chat"
    And   I see the `main title` is "Please log in to start to plan your adventure!" 
    And   I see the title is "polli-chat-app"
    And   I see the `login button`

  Scenario: User can see login modal
    Given I open the `start page`
    When  I click the `login button`
    Then  I see the `modal title` is "Who's there?"
    And   I see the `username input`
    And   I see the `password input`

  Scenario: User can see register modal
    Given I open the `start page`
    When  I click the `login button`
    And   I click the `register button`
    Then  I see the `modal title` is "Please input your data to register"
    And   I see the `username input`
    And   I see the `password input`

  Scenario: User registers with invalid input
    Given I open the `start page`
    And   I click the `login button`
    And   I click the `register button`
    When  I enter a username "testuser" into `username input`
    And   I click the `submit button`
    Then  I see the `error message` is "Please input a password!"
    But   I don't see a `success message`

  Scenario: User registers without input
    Given I open the `start page`
    And   I click the `login button`
    And   I click the `register button`
    And   I click the `submit button`
    Then  I see the `error message` is "Please input a username!"
    Then  I see the `error message` is "Please input a password!"
    But   I don't see a `success message`

  Scenario: User registers in with invalid password confirm
    Given I open the `start page`
    And   I click the `login button`
    And   I click the `register button`
    When  I enter a valid username "testuser" into `username input`
    And   I enter a valid password "test" into `password input`
    And   I enter a valid password "test another" into `password confirm input`
    And   I click the `submit button`
    Then  I see the `error message` is "Please confirm your password, they do not match!"
    But   I don't see a `success message`

  Scenario: User loggs in with invalid input
    Given I open the `start page`
    And   I click the `login button`
    When  I enter a invalid username "testuser" into `username input`
    And   I click the `submit button`
    Then  I see the `error message` is "Please input your password!"
    But   I don't see a `success message`

  Scenario: User loggs in without input
    Given I open the `start page`
    And   I click the `login button`
    And   I click the `submit button`
    Then  I see the `error message` is "Please input your username!"
    Then  I see the `error message` is "Please input your password!"
    But   I don't see a `success message`

  Scenario: User loggs in with valid input
    Given I open the `start page`
    And   I click the `login button`
    When  I enter a valid username "cucumber" into `username input`
    And   I enter a valid password "cucumber" into `password input`
    And   I click the `submit button`
    But   I don't see an `error message`


