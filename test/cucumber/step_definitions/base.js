const { client } = require('nightwatch-cucumber');
const { Given, Then, When } = require('cucumber');

const pages = {
  'start page': `${client.launch_url}/`,
};

const elements = {
  'nav-bar title': 'nav.navbar h1',
  'main title': '.main-area h1',
  'modal title': 'h1.modal-card-title',

  'login button': '.button.login',
  'register button': '.button.register',
  'submit button': 'button.is-primary',
  'new conversation button': '.button.new-conversation',
  
  'username input': 'input#auth-username',
  'password input': 'input#auth-password',
  'password confirm input': 'input#auth-password-confirm',
  
  'error message': '.error-messages',
  'success message': '.success-messages',
};


Given(/^I open the `(.*?)`$/, async (pageName) =>
  await client.url(pages[pageName]));

Then(/^I see.*? `(.*?)` is "(.*?)"$/, async (elementName, text) =>
  await client.expect.element(elements[elementName]).text.to.contain(text));

Then(/^I see.*? `(.*?)`$/, async (elementName) =>
  await client.expect.element(elements[elementName]).to.be.visible);

Then(/^I don't see.*? `(.*?)`.*?$/, async (elementName) =>
  await client.expect.element(elements[elementName]).to.not.be.visible);

Then(/^I enter.*? "(.*?)" into `(.*?)`$/, async (value, elementName) =>
  await client.setValue(elements[elementName], value));

Then(/^I click.*? `(.*?)`$/, async (elementName) =>
  await client.click(elements[elementName]));

Then(/^I click.*? `(.*?)` and wait some time$/, async (elementName) =>
  await client.click(elements[elementName]).pause(1000));

Then(/^I click.*? `(.*?)` and wait a short time$/, async (elementName) =>
  await client.click(elements[elementName]).pause(500));
