const { client } = require('nightwatch-cucumber');
const { Given, Then, When } = require('cucumber');


Then(/^I see the conversation area$/, async () =>{
  await client.expect.element('.conversation-area').to.be.visible
  await client.expect.element('.conversation-list').to.be.visible
});

 When('I see the `modal title` placeholder is {string}', async (string) =>{
  await client.expect.element('.conversation-area').to.be.visible
  await client.expect.element('h1.modal-card-title').attribute('placeholder').to.contain(string)
});

