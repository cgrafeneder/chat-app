const { client } = require('nightwatch-cucumber');
const { Given, Then, When } = require('cucumber');


Then('I see the title is {string}', async (text) => {
  await client.assert.title(text);
});
